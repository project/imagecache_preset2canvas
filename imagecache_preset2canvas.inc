<?php

/**
 * @file
 * The image cache action callback functions
 */

/**
 * ImageCache callback
 */
function imagecache_preset2canvas_preset2canvas_form($action) {
  if (imageapi_default_toolkit() != 'imageapi_gd') {
    drupal_set_message(t('Overlays are not currently supported by using imagemagick. This effect requires GD image toolkit only.'), 'warning');
  }

  $action += array(
    'xpos'  => 'left',
    'ypos'  => 'bottom',
    'alpha' => 100,
  );

  $form = array();

  $form['presetname'] = array(
    '#type'          => 'select',
    '#title'         => t('Preset'),
    '#options'       => array('' => t('- none -')),
    '#default_value' => $action['presetname'],
    '#required'      => TRUE,
  );

  $presets = imagecache_presets();
  unset($presets[arg(3)]);
  foreach ($presets as $preset) {
    $form['presetname']['#options'][$preset['presetname']] = $preset['presetname'];
  }

  $form['xpos'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Horizontal position'),
    '#default_value'    => $action['xpos'],
    '#size'             => 6,
    '#description'      => t('Enter an offset in pixels or use a keyword: <em>left</em>, <em>center</em>, or <em>right</em>.'),
    '#element_validate' => array('imagecache_preset2canvas_validate_number'),
  );

  $form['ypos'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Vertical position'),
    '#default_value'    => $action['ypos'],
    '#size'             => 6,
    '#description'      => t('Enter an offset in pixels or use a keyword: <em>top</em>, <em>center</em>, or <em>bottom</em>.'),
    '#element_validate' => array('imagecache_preset2canvas_validate_number'),
  );

  $form['alpha'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Opacity'),
    '#default_value'    => $action['alpha'],
    '#size'             => 6,
    '#description'      => t('Opacity: 0-100. <b>Warning:</b> Due to a limitation in the GD toolkit, using an opacity other than 100% requires the system to use an algorithm that\'s much slower than the built-in functions. If you want partial transparency, you are better to use an already-transparent png as the overlay source image.'),
    '#element_validate' => array('imagecache_preset2canvas_validate_alpha'),
  );

  return $form;
}

function imagecache_preset2canvas_validate_number(&$element, &$form_state) {
  if(empty($element['#value'])) form_set_value($element, 0, $form_state);
}

function imagecache_preset2canvas_validate_alpha(&$element, &$form_state) {
  if (!is_numeric($element['#value']) || $element['#value'] < 1 || $element['#value'] > 100) {
    form_set_error(implode('][', $element['#parents']), t('Opacity must be a number between 1 and 100.'));
  }
}

function theme_imagecache_preset2canvas_preset2canvas($element) {
  return t(
    'Preset: %preset, Horizontal position: %xpos, Vertical position: %ypos, Opacity: %alpha',
    array(
      '%preset' => $element['#value']['presetname'],
      '%xpos'   => $element['#value']['xpos'],
      '%ypos'   => $element['#value']['ypos'],
      '%alpha'  => $element['#value']['alpha'],
    )
  );
}

/**
 * Implementation of hook_image().
 */
function imagecache_preset2canvas_preset2canvas_image(&$image, $action = array()) {
  $overlay_src = $image->source;
  $overlay_dst = imagecache_create_path($action['presetname'], $overlay_src);
  $overlay_preset = imagecache_preset_by_name($action['presetname']);

  if (!$overlay_preset) {
    return $image;
  }

  if (!is_file($overlay_dst)) {
    imagecache_build_derivative($overlay_preset['actions'], $overlay_src, $overlay_dst);
  }

  if (is_file($overlay_dst)) {
    $overlay_image = imageapi_image_open($overlay_dst, $image->toolkit);
    return imageapi_image_overlay($image, $overlay_image, $action['xpos'], $action['ypos'], $action['alpha']);
  }
  else {
    return $image;
  }
}