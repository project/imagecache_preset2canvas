The main idea is available here:
http://www.sohtanaka.com/web-design/greyscale-hover-effect-w-css-jquery/
http://www.sohtanaka.com/web-design/examples/hover-over-trick/

Usage:
Create 2 imagecache preset:

preset name = blink_gray
  Actions:
    scale
      width = 150px

    Desaturate
      no settings (imagecache_actions color actions module required)

preset name = blink_sprites
  Actions:
    scale
      width = 150px (Same as above)

    Define Canvas
      HEX      = empty (or any color)
      width    = 100%
      height   = 200%
      X offset = left
      Y offset = bottom

    Add Overlay: same image to canvas
      Preset              = blink_gray
      Horizontal position = left
      Vertical position   = top
      Opacity             = 100



Add this PHP code to your theme template.php, without open and close tags.
In this example the name of the theme is garland,
replace this name to your theme name.
<?php
function phptemplate_imagecache($presetname, $path, $alt = '', $title = '', $attributes = NULL, $getsize = TRUE) {
  $blink_sparks = in_array($presetname, array('blink_sprites'));
  if ($blink_sparks) {
    $getsize = TRUE;
  }

  // Check is_null() so people can intentionally pass an empty array of
  // to override the defaults completely.
  if (is_null($attributes)) {
    $attributes = array('class' => 'imagecache imagecache-'. $presetname);
  }
  $image_info = NULL;
  if ($getsize && ($image_info = image_get_info(imagecache_create_path($presetname, $path)))) {
    $attributes['width'] = $image_info['width'];
    $attributes['height'] = $image_info['height'];
  }

  $attributes = drupal_attributes($attributes);
  $imagecache_url = imagecache_create_url($presetname, $path);
  $img = '<img src="'. $imagecache_url .'" alt="'. check_plain($alt) .'" title="'. check_plain($title) .'" '. $attributes .' />';

  return ($blink_sparks) ?
    _garland_sprites_image($img, $image_info)
    :
    $img
  ;
}

function _garland_sprites_image($img, $image_info) {
  static $first = TRUE;

  if (!$image_info) {
    return $img;
  }

  if ($first) {
    $first = FALSE;
    $path = drupal_get_path('theme', 'garland');
    drupal_add_css($path .'/sprites.css');
    drupal_add_js($path .'/sprites.js');
  }

  return sprintf(
    '<div class="sprites-wrapper" style="width:%dpx; height: %dpx;">%s</div>',
    $image_info['width'],
    $image_info['height'] / 2,
    $img
  );
}
?>

Content of the sprites.css file:
.sprites-wrapper {
  overflow: hidden;
}
Better solution to add this code to the style.css
and remove the
drupal_add_css($path .'/sprites.css');
line from PHP code

Content of the sprites.js file:
$(document).ready(function() {
  $(".sprites-wrapper").hover(
    //On hover...
    function() {
      //Get image url and assign it to 'thumbOver'
      var thumbOver = $(this).find("img").attr("src");

      //Set a background image(thumbOver) on the <a> tag
      $(this).css({'background' : 'url(' + thumbOver + ') no-repeat center bottom'});

      // Fade the image to 0
      // Hide the image after fade
      $(this).find("img").stop().fadeTo('normal', 0, function() { $(this).hide(); });
    },
    //on hover out...
    function() {
      //Fade the image to 1
      $(this).find("img").stop().fadeTo('normal', 1).show();
    }
  );
});